# ADS_Researchmap

ADSのbibcodeからResearchmapに登録可能なCSVファイルを吐くプログラム

# ADS API
  
まずはADS APIのtokenを手に入れる必要があり。 
[ADS](https://ui.adsabs.harvard.edu//#)にログインして(アカウントがない場合は作って)  
右上のAccount → Customize Settings → API Tokeと行って、Generate a new keyを押す。  
すると、左側にTokenが表示されるので控えておく。  
  
### sample.py
ひとまず実行したい場合は、sample.pyを編集して、実行  
  
	token: 控えたtoken
	bibcodes: csvに登録した論文のbibcode。ADSで検索する
	filename: 出力するCSVファイル名
	
### 進行中
雑誌がほとんど登録してありません。うまく登録されない場合は手動でCSVを編集するかresearch_csv.pyを変更してください