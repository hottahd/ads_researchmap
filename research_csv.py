# -*- coding: utf-8 -*-
def out(bibcodes,filename,token):
    import requests
    import json

    cbib = 'bibcode\n'+'\n'.join(bibcodes)

    f = open(filename,mode='w')
    flow = '"タイトル(日本語)","タイトル(英語)","著者(日本語)","著者(英語)","誌名(日本語)","誌名(英語)","巻","号","開始ページ","終了ページ","出版年月","査読の有無","招待の有無","記述言語","掲載種別","ISSN","ID:DOI","ID:JGlobalID","ID:NAID(CiNiiのID)","ID:PMID","Permalink","URL","概要(日本語)","概要(英語)"\n'
    f.write(flow)

    L = requests.post("https://api.adsabs.harvard.edu/v1/search/bigquery",\
                      params={"q":"*:*", "fl": "title,author,bibstem,bibstem_facet \
                      ,volume,issue,property,lang,page,page_range,year,pubdate,doi,issn,", "rows":100},\
                      headers={'Authorization': 'Bearer ' + token},\
                      data=cbib)

    for n in range(len(bibcodes)):

        article = L.json()['response']['docs'][n]
        title = '"'+article['title'][0][:]+'"'
        author = '"'+','.join(article['author'][:])+'"'

        journal = '""'
        if bibcodes[n].find('ApJ') > -1:
            journal = '"The Astrophysical Journal"'
        
        if bibcodes[n].find('PASJ') > -1:
            journal = '"Publications of the Astronomical Society of Japan"'
            
        if bibcodes[n].find('A&A') > -1:
            journal = '"Astronomy & Astrophysics"'

        if bibcodes[n].find('MNRAS') > -1:
            journal = '"Monthly Notices of the Royal Astronomical Society"'

        if bibcodes[n].find('SoPh') > -1:
            journal = '"Solar Physics"'
                
        if bibcodes[n].find('Sci') > -1:
            journal = '"Science"'

        if bibcodes[n].find('SSRv') > -1:
            journal = '"Space Science Reviews"'

        if bibcodes[n].find('Natur') > -1:
            journal = '"Nature"'

        if bibcodes[n].find('NatCo') > -1:
            journal = '"Nature Communications"'
        
        if 'volume' in article:
            volume = '"'+article['volume']+'"'
        else:
            volume = '""'

        if 'issue' in article:
            issue = '"'+article['issue']+'"'
        else:
            issue = '""'

        if "page_range" in article:
            pages = '"'+article["page_range"].split('-')[0]+'"'
            pagee = '"'+article["page_range"].split('-')[1]+'"'
        else:
            pages = '"'+article['page'][0]+'"'
            pagee = '""'

        if 'year' in article:
            year = '"'+article['year']+'"'
        else:
            year = '""'

        if 'pubdate' in article:
            date = '"'+article['pubdate'].replace('-','')+'"'
        else:
            data = '""'

        if 'REFEREED' in article['property']:
            referee = '"1"'
        else:
            referee = '"0"'

        if 'issn' in article:
            issn = '"'+article['issn']+'"'
        else:
            issn = '""'

        if 'doi' in article:
            doi = '"'+article['doi'][0]+'"'
        else:
            doi = '""'
    
        invite = '""'

        if 'lang' in article:
            lang = '"'+article['lang']+'"'
        else:
            lang = '"en"'

        URL = '"https://ui.adsabs.harvard.edu//#abs/'+bibcodes[n]+'"'


        f.write(title+','+title+','+author+','+author+','+journal+','+journal+','+volume+','+issue+ \
                ','+pages+','+pagee +','+date  +','+referee+','+invite +','+lang  +','+'""' + \
                ','+issn +','+doi   +','+'""'  +','+'""'   +','+'""'   +','+'""'  +','+URL  +','+'""' +','+'""'+'\n')
    f.close()
